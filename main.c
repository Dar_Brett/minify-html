#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/stat.h>

const int ARGS_INPUT_FILE = 1;
const char* READONLY = "rb";
const int MAX_FILE_SIZE = 8192;
int lineNumber = 0;
int column = 0;

//parseState g_state = WhiteSpace;


bool validateParameters(int argc, char** args)
{
        if(argc <= 1) {
                fprintf(stderr, "ERR_INIT: Not enough parameters\n");
                return false;
        } 
        if( access( args[ARGS_INPUT_FILE], F_OK ) == -1 ) {
                fprintf(stderr, "ERR_INIT: File not found '%s'\n", args[1]);
                return false;
        }
        if( access( args[ARGS_INPUT_FILE], R_OK ) == -1 ) {
                fprintf(stderr, "ERR_INIT: Insufficient permissions '%s'\n", 
                        args[1]);
                return false;
        }

        fprintf(stderr, "INFO: Parameter validation succeeded\n");
        return true;
}

typedef bool (*contextFunction)(char);
enum parseState {Tag, WhiteSpace};

bool isWhiteSpace(const char const * c) {
    return 
    *c == '\t' ||
    *c == '\r' ||
    *c == '\n' ||
    *c == ' ';
}
bool searchingForTag(char x)
{
        return true;
}
int skipWhiteSpace(char** c, int max) 
{
    int i = 0;
    if(i + 1 != max && !isWhiteSpace(&(*c)[i])) {
        return i;   
    }
    do {
        ++i;
        ++column;
    } while(i + 1 != max && isWhiteSpace(&(*c)[i+1]));

    *c = *c + i;
    return i;
}
bool findEndOfTag(char* current, long int toEOF, 
                  char** tagEnd, long int* remaining, long int* tagLength)
{
    int i;
    for(i = 0; i < toEOF; ++i) {
            switch(current[i])
            {
                    case '\r':
                    {
                        if(i+1 != toEOF && current[i+1] == '\n') {
                            ++i;
                        }
                    }
                    case '\n':
                    {
                        ++lineNumber;
                        column = 0;
                    }
                    case ' ':
                    case '\t':
                    {   
                        fprintf(stdout, " ");
                        if(i + 1 != toEOF && !isWhiteSpace(&current[i+1])) {
                            ++column;
                            break;   
                        }
                        do {
                            ++i;
                            ++column;
                        } while(i + 1 != toEOF && isWhiteSpace(&current[i+1]));
                        break;
                    }
                    default:
                    {
                        ++column;
                        fprintf(stderr, "%c", current[i]);
                    }
                }
            }
            fprintf(stderr, "%d:%d\n",column, i );

}
bool findNextTagOpening(char* current, long int toEOF, 
                        char** tagStart, long int* remaining)
{
        int i;
        for(i = 0; i < toEOF; ++i) {
                switch(current[i])
                {
                        case '\r':
                        {
                            if(i+1 != toEOF && current[i+1] == '\n') {
                                ++i;
                            }
                        }
                        case '\n':
                        {
                            ++lineNumber;
                            column = 0;
                        }
                        case ' ':
                        case '\t':
                        {   
                            fprintf(stdout, " ");
                            if(i + 1 != toEOF && !isWhiteSpace(&current[i+1])) {
                                ++column;
                                break;   
                            }
                            do {
                                ++i;
                                ++column;
                            } while(i + 1 != toEOF && isWhiteSpace(&current[i+1]));
                            break;
                        }
                        case '<':
                        {
                            ++column;
                            *tagStart = current;
                            *remaining = toEOF - i;
                            return true;
                        }
                        default:
                        {

                            fprintf(stderr, "ERR_PARSE: (%d, %d) Expected '<', but found '%c'\n", 
                                    lineNumber, column, *tagStart);
                            ++column;
                            *tagStart = current;
                            *remaining = toEOF - i;
                            return false;
                        }
                }
        }
}
bool parseNextTag(char* current, long int toEOF)
{
    char* tagStart;
    char* tagEnd; 
    long int tagLength;
    long int fromTagToEOF;
    if(!findNextTagOpening(current,toEOF, &tagStart, &fromTagToEOF)) {
        return false;
    }
    if(!findEndOfTag(tagStart, fromTagToEOF, &tagEnd, 
                     &fromTagToEOF, &tagLength)){
        return false;
    }
    return true;
}

bool doMinification(char* file, long int size) 
{   
        int bytesParsed = skipWhiteSpace(&file, size);
        printf("Skipped %d WhiteSpace characters\n", bytesParsed);

        int i;
        for(i = 0; i < size; ++i) {
                //fprintf(stderr, "file[%d]: %d\n", i, file[i]);
        }

        return parseNextTag(file, size - bytesParsed);
}

int fileSize(char* filename)
{
    struct stat sb;
    if(stat(filename,&sb) == -1) {
        fprintf(stderr, 
                "ERR_RUNTIME: Could not determin file size.\n");
        return -1;
    }
    return sb.st_size;
}

bool minifyHtmlFile(char* filename)
{

        // Check file size
        long int size = fileSize(filename);
        if(size > MAX_FILE_SIZE) {
                fprintf(stderr, 
                        "ERR_RUNTIME: File was too large %d bytes, max is %d\n",
                        size, MAX_FILE_SIZE);
                return false;
        }

        // Open file
        FILE* file = fopen(filename, READONLY);
        if(file == NULL) {
                fprintf(stderr, "ERR_RUNTIME: Read Error '%s'\n", filename);
                return false;
        }
        fprintf(stderr, "INFO: '%s' opened for minification\n", filename);

        // Read file into array
        char* file_arr = malloc(size * sizeof(char));

        fread(file_arr, sizeof(char), size, file);

        int i;
        for(i = 0; i < size; ++i) {
                fprintf(stderr, "file_arr[%d]: %d\n", i, file_arr[i]);
        }

        // Close file
        if(fclose(file) != 0) {
                fprintf(stderr, "ERR_RUNTIME: Unable to close file '%s'\n", 
                        filename);
                return false;
        }
        // Do work
        bool success = doMinification(file_arr, size);
        free(file_arr);
        return success;
}

int main(int argc, char** args)
{
        if(!validateParameters(argc, args))
                return 1;

        bool success = minifyHtmlFile(args[ARGS_INPUT_FILE]);

        if(success) {
                return 0;
        } else {
                return 1;
        }
}